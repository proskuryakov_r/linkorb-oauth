Symfony OAuth application
========================

This application is OAuth 2 protocol implementation with token introspection endpoint ( according to RFC 7662 ). In future it could be extended to OpenId Connect ( https://auth0.com/docs/protocols/oidc ).

Requirements
------------

  * Docker engine

Installation
------------

```bash
cd oauth/
docker-compose up -d
docker-compose exec php bash
cd oauth/
composer install
php bin/console doctrine:schema:update
```

Usage
-----

There's no need to configure anything to run the application. Just  access the application in your
browser at <http://localhost:8080>.

For XDebug change XDEBUG_CONFIG in  docker-compose: 
```yaml
    environment:
      XDEBUG_CONFIG: remote_host={{your_host}}
      PHP_IDE_CONFIG: "serverName=oauth"
```
This app has integrated with userbase application so it's important to specify proper APP_USERBASE_DSN environment variable.

Client generation
-----------------
For registering client you need to run following command
```bash
php bin/console fos:oauth-server:create-client [--redirect-uri=...] [--grant-type=...]
```

Introspection endpoint
----------------------
When client submit inquired token to resource server that's important to approve that token is vaild and has appropriate scope. For this purpose introspection endpoint has implemented ( https://tools.ietf.org/search/rfc7662 ).
Resource server must call POST /oauth/v2/token/introspect with:
token={{your_access_token}}&token_type_hint=access_token 

Tests
-----

Execute this command to run tests:

```bash
$ cd symfony-demo/
$ ./vendor/phpunit/phpunit/phpunit -c phpunit.xml.dist
```