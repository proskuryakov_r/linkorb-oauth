<?php
declare(strict_types=1);


namespace App\Entity\OAuth;

use FOS\OAuthServerBundle\Entity\AccessToken as FosOauthAccessToken;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AccessToken
 * @package App\Entity\OAuth
 * @ORM\Entity
 */
class AccessToken extends FosOauthAccessToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OAuth\Client")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $client;

    /**
     * @ORM\Column(type="string", name="username")
     */
    protected $user;

    public function getUser()
    {
        return (new \UserBase\Client\Model\User($this->user));
    }

    public function setUser(UserInterface $user)
    {
        $this->user = $user->getUsername();

        return $this;
    }
}