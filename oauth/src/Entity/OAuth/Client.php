<?php
declare(strict_types=1);


namespace App\Entity\OAuth;

use FOS\OAuthServerBundle\Entity\Client as FosOauthClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Client
 * @package App\Entity\OAuth
 * @ORM\Entity
 */
class Client extends FosOauthClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
    }
}