<?php declare(strict_types=1);

namespace App\Entity\User;

class Credentials
{
    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $csrfToken;

    /**
     * Credentials constructor.
     * @param string $username
     * @param string $password
     * @param string $csrfToken
     */
    public function __construct(string $username, string $password, string $csrfToken)
    {
        $this->username = $username;
        $this->password = $password;
        $this->csrfToken = $csrfToken;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getCsrfToken(): string
    {
        return $this->csrfToken;
    }
}