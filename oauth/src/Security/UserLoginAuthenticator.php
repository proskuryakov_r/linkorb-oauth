<?php declare(strict_types=1);

namespace App\Security;

use App\Entity\User\Credentials;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class UserLoginAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    /** @var CsrfTokenManagerInterface */
    private $csrfTokenManager;

    /** @var PasswordEncoderInterface */
    private $encoder;

    /** @var RouterInterface */
    private $router;

    /**
     * UserLoginAuthenticator constructor.
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @param PasswordEncoderInterface $encoder
     * @param RouterInterface $router
     */
    public function __construct(
        CsrfTokenManagerInterface $csrfTokenManager,
        PasswordEncoderInterface $encoder,
        RouterInterface $router
    ) {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->encoder = $encoder;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return 'user_login' === $request->attributes->get('_route')
            && $request->isMethod(Request::METHOD_POST);
    }

    /**
     * @param Request $request
     * @return Credentials
     */
    public function getCredentials(Request $request)
    {
        $credentials = new Credentials(
            $request->request->get('_username', ''),
            $request->request->get('_password', ''),
            $request->request->get('_csrf_token')
        );

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials->getUsername()
        );

        return $credentials;
    }

    /**
     * @param Credentials $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials->getCsrfToken());
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        return $userProvider->loadUserByUsername($credentials->getUsername());
    }

    /**
     * @param Credentials $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->encoder->isPasswordValid($user->getPassword(), $credentials->getPassword(), $user->getSalt());
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);

        return new RedirectResponse($targetPath ?? $this->router->generate('fos_oauth_server_authorize'));
    }

    /**
     * @return string
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('user_login');
    }
}