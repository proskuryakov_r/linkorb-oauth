<?php

namespace App\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityControllerTest extends TestCase
{
    public function testLogin()
    {
        $username = 'test';
        $errorDescription = 'Something went wrong';

        /** @var MockObject|SecurityController $controller */
        $controller = $this->createPartialMock(SecurityController::class, ['render']);

        $authUtils = $this->createConfiguredMock(
            AuthenticationUtils::class,
            [
                'getLastUsername' => $username,
                'getLastAuthenticationError' => [$errorDescription]
            ]
        );

        $controller->expects($this->once())->method('render')->with(
            'security/login.html.twig',
            [
                'last_username' => $username,
                'error' => [$errorDescription]
            ]
        );

        $controller->login($authUtils);
    }
}
