<?php

namespace App\Security;

use App\Entity\User\Credentials;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class UserLoginAuthenticatorTest extends TestCase
{
    /** @var UserLoginAuthenticator */
    private $authenticator;

    /** @var CsrfTokenManagerInterface|MockObject */
    private $csrfManager;

    /** @var PasswordEncoderInterface|MockObject */
    private $encoder;

    /** @var RouterInterface|MockObject */
    private $router;

    protected function setUp()
    {
        $this->csrfManager = $this->createMock(CsrfTokenManagerInterface::class);
        $this->encoder = $this->createMock(PasswordEncoderInterface::class);
        $this->router = $this->createMock(RouterInterface::class);

        $this->authenticator = new UserLoginAuthenticator($this->csrfManager, $this->encoder, $this->router);
    }

    public function testOnAuthenticationSuccess()
    {
        $session = $this->createConfiguredMock(SessionInterface::class, ['get' => '1234qwer']);
        $provider = 'cookie';

        $request = $this->createConfiguredMock(Request::class, ['getSession' => $session]);

        $this->assertInstanceOf(RedirectResponse::class, $this->authenticator->onAuthenticationSuccess($request, $this->createMock(TokenInterface::class), $provider));
    }

    public function testGetCredentials()
    {
        $username = 'test';
        $password = '123Qwe';
        $csrfToken = 'cookie-x-token';

        $session = $this->createMock(SessionInterface::class);
        $request = $this->createMock(Request::class);
        $request->method('getSession')->willReturn($session);
        $post = $this->createMock(ParameterBagInterface::class);
        $post
            ->method('get')
            ->willReturnMap([
                ['_username', '', $username],
                ['_password', '', $password],
                ['_csrf_token', $csrfToken],
            ]);
        $request->request = $post;

        $session->expects($this->once())->method('set')->with(Security::LAST_USERNAME, $username);

        $this->assertEquals(new Credentials($username, $password, $csrfToken), $this->authenticator->getCredentials($request));
    }

    public function testSupports()
    {
        $request = $this->createMock(Request::class);
        $attr = $this->createConfiguredMock(ParameterBagInterface::class, ['get' => 'user_login']);
        $request->attributes = $attr;
        $request->method('isMethod')->with(Request::METHOD_POST)->willReturn(true);

        $this->assertTrue($this->authenticator->supports($request));
    }

    public function testGetUser()
    {
        $token = 'x-csrf';
        $username = 'test';

        $credentials = $this->createConfiguredMock(
            Credentials::class,
            ['getUsername' => $username, 'getCsrfToken' => $token]
        );

        $this->csrfManager
            ->expects($this->once())
            ->method('isTokenValid')
            ->with(new CsrfToken('authenticate', $token))
            ->willReturn(true);

        $userProvider = $this->createMock(UserProviderInterface::class);
        $userProvider->expects($this->once())->method('loadUserByUsername')->with($username);

        $this->authenticator->getUser($credentials, $userProvider);
    }

    public function testGetUserException()
    {
        $token = 'x-csrf';
        $username = 'test';

        $credentials = $this->createConfiguredMock(
            Credentials::class,
            ['getUsername' => $username, 'getCsrfToken' => $token]
        );

        $this->csrfManager
            ->expects($this->once())
            ->method('isTokenValid')
            ->with(new CsrfToken('authenticate', $token))
            ->willReturn(false);

        $userProvider = $this->createMock(UserProviderInterface::class);
        $this->expectException(InvalidCsrfTokenException::class);

        $this->authenticator->getUser($credentials, $userProvider);
    }

    public function testCheckCredentials()
    {
        $password = 'password';
        $encodedPassword = md5('password');
        $salt = 'testSalt';

        $credentials = $this->createConfiguredMock(Credentials::class, [
            'getPassword' => $encodedPassword,
        ]);

        $user = $this->createConfiguredMock(UserInterface::class, [
            'getSalt' => $salt,
            'getPassword' => $password
        ]);

        $this->encoder
            ->expects($this->once())
            ->method('isPasswordValid')
            ->with($password, $encodedPassword, $salt);

        $this->authenticator->checkCredentials($credentials, $user);
    }
}
