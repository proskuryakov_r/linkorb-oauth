<?php

namespace App\Entity\User;

use PHPUnit\Framework\TestCase;

class CredentialsTest extends TestCase
{
    public function testGetUsername()
    {
        $username = 'qwerty';

        $this->assertEquals($username, (new Credentials($username, '', ''))->getUsername());
    }

    public function testGetPassword()
    {
        $pass = '123Qwe';

        $this->assertEquals($pass, (new Credentials('', $pass, ''))->getPassword());
    }

    public function testGetCsrfToken()
    {
        $token = 'x-token';

        $this->assertEquals($token, (new Credentials('', '', $token))->getCsrfToken());
    }
}
