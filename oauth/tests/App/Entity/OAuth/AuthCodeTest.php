<?php

namespace App\Entity\OAuth;

use App\Tests\Entity\OAuth\UserAwareTrait;
use PHPUnit\Framework\TestCase;

class AuthCodeTest extends TestCase
{
    use UserAwareTrait;

    public function setUp()
    {
        $this->setUserMemento(new AuthCode);
    }
}
