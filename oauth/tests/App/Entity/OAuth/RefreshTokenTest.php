<?php

namespace App\Entity\OAuth;

use App\Tests\Entity\OAuth\UserAwareTrait;
use PHPUnit\Framework\TestCase;

class RefreshTokenTest extends TestCase
{
    use UserAwareTrait;

    public function setUp()
    {
        $this->setUserMemento(new RefreshToken);
    }
}
