<?php

namespace App\Tests\Entity\OAuth;

use App\Entity\OAuth\AccessToken;
use App\Entity\OAuth\AuthCode;
use App\Entity\OAuth\RefreshToken;
use UserBase\Client\Model\User;

trait UserAwareTrait
{
    /** @var object|AccessToken|AuthCode|RefreshToken */
    private $userMemento;

    public abstract function assertEquals($expected, $actual, string $message = '', float $delta = 0.0, int $maxDepth = 10, bool $canonicalize = false, bool $ignoreCase = false): void;

    public function setUserMemento($userMemento)
    {
        $this->userMemento = $userMemento;
    }

    public function testGetterAndSetterUser()
    {
        $username = 'test3';

        $this->userMemento->setUser(new User($username));
        $this->assertEquals(new User($username), $this->userMemento->getUser());
    }
}