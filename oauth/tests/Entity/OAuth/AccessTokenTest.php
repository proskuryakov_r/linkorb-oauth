<?php

namespace App\Tests\Entity\OAuth;

use App\Entity\OAuth\AccessToken;
use PHPUnit\Framework\TestCase;

class AccessTokenTest extends TestCase
{
    use UserAwareTrait;

    protected function setUp()
    {
        $this->setUserMemento(new AccessToken);
    }
}
